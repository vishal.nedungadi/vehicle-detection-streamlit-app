import streamlit as st
from PIL import Image
import argparse
from timeit import default_timer as timer
from yolo import YOLO
import math
import colorsys
import os
import glob
import numpy as np
from keras import backend as K
from keras.models import load_model
from keras.layers import Input
from PIL import Image, ImageFont, ImageDraw
from typing import Dict
from yolo3.model import yolo_eval, yolo_body, tiny_yolo_body
from yolo3.utils import letterbox_image
from keras.utils import multi_gpu_model
import cv2


st.set_option('deprecation.showfileUploaderEncoding', False)
st.markdown("<h1 style='text-align: center;'>Vehicle Detection</h1>", unsafe_allow_html=True)
st.sidebar.title("Settings")
fr= st.sidebar.slider("Output video duration in seconds", 1, 20, 2, 1)
fps= 2
progress_bar = st.sidebar.progress(0)
frame_text = st.sidebar.empty()
det=st.sidebar.empty()
crop_text=st.empty()
crops=st.empty()
im = st.empty()
text1=st.empty()
image1 = st.empty()
text2=st.empty()
image2 = st.empty()
text3=st.empty()
image3 = st.empty()
run = False
def disp():
    text1.subheader("Original Video")
    text2.subheader("Predicted Video")
    image1.video(open('static/orgvid.webm', 'rb').read())
    image2.video(open('static/predvid.webm', 'rb').read())

@st.cache(allow_output_mutation = True, suppress_st_warning=True)
def load_model():
    parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS)
    parser.add_argument(
        '--model', type=str,
        help='path to model weight file, default ' + YOLO.get_defaults("model_path")
    )

    parser.add_argument(
        '--anchors', type=str,
        help='path to anchor definitions, default ' + YOLO.get_defaults("anchors_path")
    )

    parser.add_argument(
        '--classes', type=str,
        help='path to class definitions, default ' + YOLO.get_defaults("classes_path")
    )

    parser.add_argument(
        '--gpu_num', type=int,
        help='Number of GPU to use, default ' + str(YOLO.get_defaults("gpu_num"))
    )
    FLAGS = parser.parse_args()
    yolo = YOLO(**vars(FLAGS))
    return yolo
def main():
    run = False
    get_file = st.empty()
    run_button = st.empty()
    files=get_file.file_uploader("Upload video:")
    show_file = st.empty()
    yolo = load_model()
    if files:
        content = files.getvalue()
        vid_sub=open("static/subvideo.mp4","wb")
        vid_sub.write(content)
        show_file.empty()
        run = run_button.button('Run Detections')
        cap=None
        cap = cv2.VideoCapture("static/subvideo.mp4")
        ret, frame = cap.read()
        video_fps = cap.get(cv2.CAP_PROP_FPS)
        cap.release()
        Width = frame.shape[1]
        Height = frame.shape[0]
        vid_info="<h2>Input video details<h2><p>FPS: "+str(math.ceil(video_fps))+"</p>"\
            "<p>Video resolution: "+str(Height)+"x"+str(Width)+"</p>"      
        show_file.markdown(vid_info,unsafe_allow_html=True) 
    if run:
        idx = 1
        cap=None
        cap = cv2.VideoCapture("static/subvideo.mp4")
        inp = cv2.VideoWriter("static/orgvid.webm",
                                cv2.VideoWriter_fourcc(*'vp80'), fps, (int(cap.get(3)), int(cap.get(4))))
        outp = cv2.VideoWriter("static/predvid.webm",
                                cv2.VideoWriter_fourcc(*'vp80'), fps, (int(cap.get(3)), int(cap.get(4))))
        start = timer()
        # timer_dict = {}
        box_dict = {}
        while(cap.isOpened()):
            count_array = {"vehicle":0, "car":0, "truck":0, "motorbike":0, "bus":0, "bicycle":0}
            cr_ar = []
            ret, frame = cap.read()
            box_list = []
            #following code is for increasing the brightness of the image before processing
            # frame = np.clip((1.3*frame + 40),0,255)
            # frame = np.uint8(frame)
            ## print(frame.shape)
            if ret is not True or idx == (fr*fps+1):
                cap.release()
                outp.release()
                inp.release()
                print("Exiting program!")
                break
            orgfr=frame.copy()
            progress_bar.progress(int(((idx-1)/(fr*fps))*100))
            frame_text.text("Frame "+str(idx)+"/"+str(fr*fps))
            image = Image.fromarray(frame)
            self,out_boxes, out_scores, out_classes = yolo.detect_image(image)
            #####  processing of boxes  #####
            print('Found {} boxes for {}'.format(len(out_boxes), 'img'))            
            font = ImageFont.truetype(font='font/FiraMono-Medium.otf',
                        size=np.floor(2e-2 * image.size[1] + 0.2).astype('int32'))
            thickness = (image.size[0] + image.size[1]) // 300
            custom_classes = ["bicycle", "car", "motorbike", "bus", "truck"]
            class_wise_score = {"truck":0.64}
            for i, c in reversed(list(enumerate(out_classes))):
                predicted_class = self.class_names[c]
                score = out_scores[i]
                if predicted_class in custom_classes:
                    if predicted_class in class_wise_score.keys() and score < class_wise_score[predicted_class]:
                        continue
                    count_array[predicted_class] += 1
                    count_array["vehicle"] += 1
                    box = out_boxes[i]
                    score = out_scores[i]
                    
                    top, left, bottom, right = box
                    top = max(0, np.floor(top + 0.5).astype('int32'))
                    left = max(0, np.floor(left + 0.5).astype('int32'))
                    bottom = min(image.size[1], np.floor(bottom + 0.5).astype('int32'))
                    right = min(image.size[0], np.floor(right + 0.5).astype('int32'))
                    #calculating centroid and time of each box
                    percent_change = 10
                    centroid = (int((left + right)/2), int((top + bottom)/2))
                    flag = True
                    if idx == 1:
                        # timer_dict[i] = 0
                        box_dict[centroid] = 0
                    else:
                        for j in box_dict.keys():
                            x, y = j
                            x1, y1 = centroid
                            percent_x = (abs(x - x1)/x) * 100
                            percent_y = (abs(y - y1)/y) * 100
                            if percent_x < percent_change and percent_y < percent_change:
                                flag = False
                                break
                        print(box_dict)
                        print(flag)
                        if flag == True:
                            box_dict[centroid] = 0
                        if flag == False:
                            print(box_dict)
                            temp_time = box_dict[j]
                            del box_dict[j]
                            box_dict[centroid] = temp_time + (1/fps)
                    print(box_dict)
                    label = '{}s | {} {:.2f}'.format(box_dict[centroid], predicted_class, score)
                    draw = ImageDraw.Draw(image)
                    label_size = draw.textsize(label, font)
                    # label_size2 = draw.textsize(label_time, font)
                    print(label, (left, top), (right, bottom))
                    if top - label_size[1] >= 0:
                        text_origin = np.array([left, top - label_size[1]])
                    else:
                        text_origin = np.array([left, top + 1])
                    for i in range(thickness):
                        draw.rectangle(
                            [left + i, top + i, right - i, bottom - i],
                            outline=self.colors[c])
                    draw.rectangle(
                        [tuple(text_origin), tuple(text_origin + label_size)],
                        fill=self.colors[c])
                    draw.text(text_origin, label, fill=(0, 0, 0), font=font)
                    del draw
            ##### processsing of boxes complete #####
            image = np.array(image)
            im = st.empty()
            text_video = "\nNo. of Vehicles: " +str(count_array["vehicle"])+ "\nNo. of Cars: " + \
                str(count_array["car"]) + "\nNo. of Trucks: " + str(count_array["truck"]) +\
                    "\nNo. of Bikes: " + str(count_array["motorbike"]) + "\nOthers: " + str(count_array["bicycle"]+count_array["bus"])
            # curr_fps = 0
            cv2.rectangle(image, (10,10), (250,130), (255,255,255),(-1))
            y0, dy = 10, 20
            for i, line in enumerate(text_video.split('\n')):
                y = y0 + i*dy
                print(line)
                cv2.putText(image, text=line, org=(10, y), fontFace=cv2.FONT_ITALIC,
                            fontScale=.75, color=(0, 0, 0), thickness=1)

            cv2.imwrite("static/fr"+str(idx)+".jpg",image)
            outp.write(image)
            inp.write(orgfr)
            idx += 1
        end = timer()    
        det_text="<p style = 'font-size:13px'>Inference time: <span>"+str(float("{:.2f}".format(end - start)))+"s</span></p>"\
            "<p style = 'font-size:13px'>FPS: "+str(math.ceil(video_fps))+ "</p>"\
                "<p style = 'font-size:13px'> Sampling Frame Rate: "+str(fps)+\
                    "<p style = 'font-size:13px'>Video resolution: "+str(Height)+"x"+str(Width)+"</p>"\
                        "<p style = 'font-size:13px'>Net Input Resolution: <span>"+"416 X 416"+"</span></p>"
        det.markdown(det_text,unsafe_allow_html=True)
        get_file.empty()
        run_button.empty()
        
        show_file.empty()
        progress_bar.progress(int(100))
        disp()
main()
