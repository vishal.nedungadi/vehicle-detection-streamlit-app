import streamlit as st
from PIL import Image
import argparse
from timeit import default_timer as timer

import math
import colorsys
import os
import glob
import numpy as np
from keras import backend as K
from keras.models import load_model
from keras.models import model_from_json
from keras.layers import Input
from PIL import Image, ImageFont, ImageDraw
from typing import Dict
from local_utils import detect_lp
from os.path import splitext,basename

from sklearn.preprocessing import LabelEncoder

from yolo3.utils import letterbox_image
from keras.utils import multi_gpu_model
import cv2


st.set_option('deprecation.showfileUploaderEncoding', False)
st.markdown("<h1 style='text-align: center;'>Plate Detection</h1>", unsafe_allow_html=True)
st.sidebar.title("Settings")
fr= st.sidebar.slider("Output video duration in seconds", 1, 20, 2, 1)
fps= 2
progress_bar = st.sidebar.progress(0)
frame_text = st.sidebar.empty()
det=st.sidebar.empty()
crop_text=st.empty()
crops=st.empty()
im = st.empty()
text1=st.empty()
image1 = st.empty()
text2=st.empty()
image2 = st.empty()
text3=st.empty()
image3 = st.empty()
run = False
def sort_contours(cnts,reverse = False):
    i = 0
    boundingBoxes = [cv2.boundingRect(c) for c in cnts]
    (cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes),
                                        key=lambda b: b[1][i], reverse=reverse))
    return cnts
def load_model(path):
    try:
        path = splitext(path)[0]
        with open('%s.json' % path, 'r') as json_file:
            model_json = json_file.read()
        model = model_from_json(model_json, custom_objects={})
        model.load_weights('%s.h5' % path)
        print("Loading model successfully...")
        # print("ahsdoihasodaksndklansd")
        return model
    except Exception as e:
        # print("not working")
        print(e)



def draw_box(img, cor, thickness=7): 
    pts=[]  
    x_coordinates=cor[0][0]
    y_coordinates=cor[0][1]
    # store the top-left, top-right, bottom-left, bottom-right 
    # of the plate license respectively
    for i in range(4):
        pts.append([int(x_coordinates[i]),int(y_coordinates[i])])
    
    pts = np.array(pts, np.int32)
    pts = pts.reshape((-1,1,2))
    # plt.imshow(img)
    # vehicle_image = preprocess_image(img)
    vehicle_image = img
    # plt.imshow(vehicle_image)
    cv2.polylines(vehicle_image,[pts],True,(0,255,0),thickness)
    return vehicle_image

def preprocess_image(img,resize=False):
    # img = cv2.imread(image_path)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img = img / 255
    if resize:
        img = cv2.resize(img, (224,224))
    return img

def get_plate(wpod_net, img, Dmax=608, Dmin = 608):
    vehicle = preprocess_image(img)
    ratio = float(max(vehicle.shape[:2])) / min(vehicle.shape[:2])
    side = int(ratio * Dmin)
    bound_dim = min(side, Dmax)
    _ , LpImg, _, cor = detect_lp(wpod_net, vehicle, bound_dim, lp_threshold=0.5)
    return vehicle, LpImg, cor

def disp():
    text1.subheader("Original Video")
    text2.subheader("Predicted Video")
    image1.video(open('static/orgvid.webm', 'rb').read())
    image2.video(open('static/predvid.webm', 'rb').read())


def main():
    wpod_net_path = "wpod-net.json"
    wpod_net = load_model(wpod_net_path)
    
    # Load model architecture, weight and labels
    json_file = open('MobileNets_character_recognition.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    model = model_from_json(loaded_model_json)
    model.load_weights("License_character_recognition_weight.h5")
    print("[INFO] Model loaded successfully...")

    labels = LabelEncoder()
    labels.classes_ = np.load('license_character_classes.npy')
    print("[INFO] Labels loaded successfully...")
    run = False
    get_file = st.empty()
    run_button = st.empty()
    files=get_file.file_uploader("Upload video:")
    show_file = st.empty()
    if files:
        content = files.getvalue()
        vid_sub=open("static/subvideo.mp4","wb")
        vid_sub.write(content)
        show_file.empty()
        run = run_button.button('Run Detections')
        cap=None
        cap = cv2.VideoCapture("static/subvideo.mp4")
        ret, frame = cap.read()
        video_fps = cap.get(cv2.CAP_PROP_FPS)
        cap.release()
        Width = frame.shape[1]
        Height = frame.shape[0]
        vid_info="<h2>Input video details<h2><p>FPS: "+str(math.ceil(video_fps))+"</p>"\
            "<p>Video resolution: "+str(Height)+"x"+str(Width)+"</p>"      
        show_file.markdown(vid_info,unsafe_allow_html=True) 
    if run:
        idx = 1
        cap=None
        cap = cv2.VideoCapture("static/subvideo.mp4")
        inp = cv2.VideoWriter("static/orgvid.webm",
                                cv2.VideoWriter_fourcc(*'vp80'), fps, (int(cap.get(3)), int(cap.get(4))))
        outp = cv2.VideoWriter("static/predvid.webm",
                                cv2.VideoWriter_fourcc(*'vp80'), fps, (int(cap.get(3)), int(cap.get(4))))
        start = timer()
        # timer_dict = {}
        box_dict = {}
        while(cap.isOpened()):
            ret, frame = cap.read()
            cv2.imwrite('sample.jpg',frame)
            if ret is not True or idx == (fr*fps+1):
                cap.release()
                outp.release()
                inp.release()
                print("Exiting program!")
                break
            idx += 1
            vehicle, LpImg,cor = get_plate(wpod_net, frame)    
            if (len(LpImg)): #check if there is at least one license image
                # Scales, calculates absolute values, and converts the result to 8-bit.
                
                plate_image = cv2.convertScaleAbs(LpImg[0], alpha=(255.0))
                
                # convert to grayscale and blur the image
                gray = cv2.cvtColor(plate_image, cv2.COLOR_BGR2GRAY)
                blur = cv2.GaussianBlur(gray,(7,7),0)
                
                # Applied inversed thresh_binary 
                binary = cv2.threshold(blur, 180, 255,
                                    cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]
                
                kernel3 = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
                thre_mor = cv2.morphologyEx(binary, cv2.MORPH_DILATE, kernel3)


            cont, _  = cv2.findContours(binary, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

            # creat a copy version "test_roi" of plat_image to draw bounding box
            test_roi = plate_image.copy()

            # Initialize a list which will be used to append charater image
            crop_characters = []

            # define standard width and height of character
            digit_w, digit_h = 30, 60

            for c in sort_contours(cont):
                (x, y, w, h) = cv2.boundingRect(c)
                ratio = h/w
                if 1<=ratio<=3.5: # Only select contour with defined ratio
                    if h/plate_image.shape[0]>=0.5: # Select contour which has the height larger than 50% of the plate
                        # Draw bounding box arroung digit number
                        cv2.rectangle(test_roi, (x, y), (x + w, y + h), (0, 255,0), 2)

                        # Sperate number and gibe prediction
                        curr_num = thre_mor[y:y+h,x:x+w]
                        curr_num = cv2.resize(curr_num, dsize=(digit_w, digit_h))
                        _, curr_num = cv2.threshold(curr_num, 220, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
                        crop_characters.append(curr_num)
            final_string = ''
            for i,character in enumerate(crop_characters):
                # fig.add_subplot(grid[i])
                title = np.array2string(predict_from_model(character,model,labels))
                # plt.title('{}'.format(title.strip("'[]"),fontsize=20))
                final_string+=title.strip("'[]")
                # plt.axis(False)
                # plt.imshow(character,cmap='gray')
            print(final_string)
            new_img = draw_box(frame, cor)
            outp.write(new_img)
            inp.write(frame)
        end = timer()    
        st.write("Predicted output: ",final_string)
        det_text="<p style = 'font-size:13px'>Inference time: <span>"+str(float("{:.2f}".format(end - start)))+"s</span></p>"\
        "<p style = 'font-size:13px'>FPS: "+str(math.ceil(video_fps))+ "</p>"\
            "<p style = 'font-size:13px'> Sampling Frame Rate: "+str(fps)+\
                "<p style = 'font-size:13px'>Video resolution: "+str(Height)+"x"+str(Width)+"</p>"
                    # "<p style = 'font-size:13px'>Net Input Resolution: <span>"+"416 X 416"+"</span></p>"
        det.markdown(det_text,unsafe_allow_html=True)
        get_file.empty()
        run_button.empty()
        
        show_file.empty()
        progress_bar.progress(int(100))
        disp()
main()
