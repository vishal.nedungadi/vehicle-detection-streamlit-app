import streamlit as st
from PIL import Image
import argparse
from timeit import default_timer as timer
from yolo import YOLO

import colorsys
import os
import glob
import numpy as np
from keras import backend as K
from keras.models import load_model
from keras.layers import Input
from PIL import Image, ImageFont, ImageDraw
from typing import Dict
from yolo3.model import yolo_eval, yolo_body, tiny_yolo_body
from yolo3.utils import letterbox_image
from keras.utils import multi_gpu_model



st.set_option('deprecation.showfileUploaderEncoding', False)
st.title('Vehicle Detection')

# @st.cache(allow_output_mutation=True)
def clear_folder():
    files = glob.glob('./static/*')
    for f in files:
        os.remove(f)

@st.cache(allow_output_mutation=True)
def get_static_store() -> Dict:
    """This dictionary is initialized once and can be used to store the files uploaded"""
    return {}
# error = st.empty()
# a = placeholder.button('Lets Begin!', key = 'placeholder_button')

# selected_frames = st.sidebar.slider("Choose the number of frames ", 0, 10, 0, key = 'slider_frame')
# placeholder.empty() 
@st.cache(allow_output_mutation = True, suppress_st_warning=True)
def load_model():
    parser = argparse.ArgumentParser(argument_default=argparse.SUPPRESS)
    parser.add_argument(
        '--model', type=str,
        help='path to model weight file, default ' + YOLO.get_defaults("model_path")
    )

    parser.add_argument(
        '--anchors', type=str,
        help='path to anchor definitions, default ' + YOLO.get_defaults("anchors_path")
    )

    parser.add_argument(
        '--classes', type=str,
        help='path to class definitions, default ' + YOLO.get_defaults("classes_path")
    )

    parser.add_argument(
        '--gpu_num', type=int,
        help='Number of GPU to use, default ' + str(YOLO.get_defaults("gpu_num"))
    )
    # print(static_store)
    FLAGS = parser.parse_args()
    yolo = YOLO(**vars(FLAGS))
    return yolo


    

# @st.cache
def main():
    # progress_bar = st.empty()
    yolo = load_model()
    static_store = get_static_store()
    # clear_folder()
    file_uploaded = st.file_uploader('Choose one or many images', type = ['jpeg','jpg'])
    placeholder = st.empty()
    if file_uploaded:
        content = file_uploaded.getvalue()
        if not content in static_store.values():
            static_store[file_uploaded] = content
        # vid_sub=open("static/uploaded.jpg","wb")
        # vid_sub.write(content)
        # placeholder.empty()

        ### YOLO ###
        
        count_files = st.text("Number of Files Uploaded: "+str(len(static_store.keys())))
        run = st.button('Run Detections')
        if run:
            
            clear_folder()
            count_files.empty()
            progress_bar = st.sidebar.progress(0)
            progress_text = st.sidebar.empty()
            j = 0
            for i in static_store.values():
                vid_sub=open("static/uploaded"+str(j)+".jpg","wb")
                vid_sub.write(i)
                j += 1
            files = glob.glob('./static/*')
            count = 0
            for f in files:
                progress_bar.progress(int(count/len(files)*100))
                progress_text.text("Processed: "+str(count) + "/"+ str(len(files)) + " Images")
                count += 1
                start = timer()
                image = Image.open(f)
                
                self,out_boxes, out_scores, out_classes = yolo.detect_image(image)

                #####  processing of boxes  #####
                print('Found {} boxes for {}'.format(len(out_boxes), 'img'))

                det = st.sidebar.empty()
                
                font = ImageFont.truetype(font='font/FiraMono-Medium.otf',
                            size=np.floor(2e-2 * image.size[1] + 0.2).astype('int32'))
                thickness = (image.size[0] + image.size[1]) // 300
                custom_classes = ["bicycle", "car", "motorbike", "bus", "truck"]
                for i, c in reversed(list(enumerate(out_classes))):
                    predicted_class = self.class_names[c]
                    
                    if predicted_class in custom_classes:
                        box = out_boxes[i]
                        score = out_scores[i]

                        label = '{} {:.2f}'.format(predicted_class, score)
                        draw = ImageDraw.Draw(image)
                        label_size = draw.textsize(label, font)

                        top, left, bottom, right = box
                        top = max(0, np.floor(top + 0.5).astype('int32'))
                        left = max(0, np.floor(left + 0.5).astype('int32'))
                        bottom = min(image.size[1], np.floor(bottom + 0.5).astype('int32'))
                        right = min(image.size[0], np.floor(right + 0.5).astype('int32'))
                        print(label, (left, top), (right, bottom))

                        if top - label_size[1] >= 0:
                            text_origin = np.array([left, top - label_size[1]])
                        else:
                            text_origin = np.array([left, top + 1])

                        # My kingdom for a good redistributable image drawing library.
                        for i in range(thickness):
                            draw.rectangle(
                                [left + i, top + i, right - i, bottom - i],
                                outline=self.colors[c])
                        draw.rectangle(
                            [tuple(text_origin), tuple(text_origin + label_size)],
                            fill=self.colors[c])
                        draw.text(text_origin, label, fill=(0, 0, 0), font=font)
                        del draw
                ##### processsing of boxes complete #####

                end = timer()
                x, y = image.size
                det_text="<br /><p style = 'color: blue;font-size:13px'> Image: <span>" + str(count)+"</span></p>"\
                "<p style = 'font-size:13px'>Inference time: <span>"+str(float("{:.2f}".format(end - start)))+"s</span></p>"\
                    "<p style = 'font-size:13px'>Raw Input Resolution: <span>"+str(y)+"X"+str(x)+"</span></p>"\
                        "<p style = 'font-size:13px'>Net Input Resolution: <span>"+"416 X 416"+"</span></p>"\
                        "<br />"

                det.markdown(det_text,unsafe_allow_html=True)
                flag = 1                
                if flag == 1:
                    st.image(image, use_column_width = True)
            progress_text.text("Processed: "+str(count) + "/"+ str(len(files)) + " Images")
            progress_bar.progress(100)
            static_store.clear()
    else:
        static_store.clear()
 

main()