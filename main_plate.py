import streamlit as st
from timeit import default_timer as timer
import cv2
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from local_utils import detect_lp
from os.path import splitext,basename
from keras.models import model_from_json
from keras.preprocessing.image import load_img, img_to_array
from keras.applications.mobilenet_v2 import preprocess_input
from sklearn.preprocessing import LabelEncoder
import glob
from typing import Dict
import os


st.set_option('deprecation.showfileUploaderEncoding', False)
st.title('Plate Detection')

# @st.cache(allow_output_mutation=True)
def clear_folder():
    files = glob.glob('./static/*')
    for f in files:
        os.remove(f)

@st.cache(allow_output_mutation=True)
def get_static_store() -> Dict:
    """This dictionary is initialized once and can be used to store the files uploaded"""
    return {}
# error = st.empty()
# a = placeholder.button('Lets Begin!', key = 'placeholder_button')

# selected_frames = st.sidebar.slider("Choose the number of frames ", 0, 10, 0, key = 'slider_frame')
# placeholder.empty() 
def load_model(path):
    try:
        path = splitext(path)[0]
        with open('%s.json' % path, 'r') as json_file:
            model_json = json_file.read()
        model = model_from_json(model_json, custom_objects={})
        model.load_weights('%s.h5' % path)
        print("Loading model successfully...")
        return model
    except Exception as e:
        print(e)
def draw_box(image_path, cor, thickness=5): 
    pts=[]  
    x_coordinates=cor[0][0]
    y_coordinates=cor[0][1]
    # store the top-left, top-right, bottom-left, bottom-right 
    # of the plate license respectively
    for i in range(4):
        pts.append([int(x_coordinates[i]),int(y_coordinates[i])])
    
    pts = np.array(pts, np.int32)
    pts = pts.reshape((-1,1,2))
    vehicle_image = preprocess_image(image_path)
    
    cv2.polylines(vehicle_image,[pts],True,(0,255,0),thickness)
    return vehicle_image

#@st.cache(allow_output_mutation = True, suppress_st_warning=True)
def load_model2():
    wpod_net_path = "wpod-net.json"
    wpod_net = load_model(wpod_net_path)
    return wpod_net

def preprocess_image(image_path,resize=False):
    img = cv2.imread(image_path)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    img = img / 255
    if resize:
        img = cv2.resize(img, (224,224))
    return img

def get_plate(wpod_net, image_path, Dmax=608, Dmin = 608):
    vehicle = preprocess_image(image_path)
    ratio = float(max(vehicle.shape[:2])) / min(vehicle.shape[:2])
    side = int(ratio * Dmin)
    bound_dim = min(side, Dmax)
    _ , LpImg, _, cor = detect_lp(wpod_net, vehicle, bound_dim, lp_threshold=0.5)
    return vehicle, LpImg, cor

def predict_from_model(image,model,labels):
    image = cv2.resize(image,(80,80))
    image = np.stack((image,)*3, axis=-1)
    prediction = labels.inverse_transform([np.argmax(model.predict(image[np.newaxis,:]))])
    return prediction   
def sort_contours(cnts,reverse = False):
    i = 0
    boundingBoxes = [cv2.boundingRect(c) for c in cnts]
    (cnts, boundingBoxes) = zip(*sorted(zip(cnts, boundingBoxes),
                                        key=lambda b: b[1][i], reverse=reverse))
    return cnts
# @st.cache
def main():
    # progress_bar = st.empty()
    wpod_net = load_model2()
    
    static_store = get_static_store()
    # clear_folder()
    file_uploaded = st.file_uploader('Choose one or many images', type = ['jpeg','jpg'])
    placeholder = st.empty()
    if file_uploaded:
        content = file_uploaded.getvalue()
        if not content in static_store.values():
            static_store[file_uploaded] = content
        # vid_sub=open("static/uploaded.jpg","wb")
        # vid_sub.write(content)
        # placeholder.empty()

        ### YOLO ###
        
        count_files = st.text("Number of Files Uploaded: "+str(len(static_store.keys())))
        run = st.button('Run Detections')
        if run:
            
            clear_folder()
            count_files.empty()
            progress_bar = st.sidebar.progress(0)
            progress_text = st.sidebar.empty()
            j = 0
            for i in static_store.values():
                vid_sub=open("static/uploaded"+str(j)+".jpg","wb")
                vid_sub.write(i)
                j += 1
            files = glob.glob('./static/*')
            count = 0
            for f in files:
                progress_bar.progress(int(count/len(files)*100))
                progress_text.text("Processed: "+str(count) + "/"+ str(len(files)) + " Images")
                det = st.sidebar.empty()
                count += 1
                start = timer()
                vehicle, LpImg,cor = get_plate(wpod_net, f)
                if (len(LpImg)): #check if there is at least one license image
                # Scales, calculates absolute values, and converts the result to 8-bit.
                    plate_image = cv2.convertScaleAbs(LpImg[0], alpha=(255.0))
                    
                    # convert to grayscale and blur the image
                    gray = cv2.cvtColor(plate_image, cv2.COLOR_BGR2GRAY)
                    blur = cv2.GaussianBlur(gray,(7,7),0)
                    
                    # Applied inversed thresh_binary 
                    binary = cv2.threshold(blur, 180, 255,
                                        cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)[1]
                    
                    kernel3 = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
                    thre_mor = cv2.morphologyEx(binary, cv2.MORPH_DILATE, kernel3)


                

                cont, _  = cv2.findContours(binary, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

                # creat a copy version "test_roi" of plat_image to draw bounding box
                test_roi = plate_image.copy()

                # Initialize a list which will be used to append charater image
                crop_characters = []

                # define standard width and height of character
                digit_w, digit_h = 30, 60

                for c in sort_contours(cont):
                    (x, y, w, h) = cv2.boundingRect(c)
                    ratio = h/w
                    if 1<=ratio<=3.5: # Only select contour with defined ratio
                        if h/plate_image.shape[0]>=0.5: # Select contour which has the height larger than 50% of the plate
                            # Draw bounding box arroung digit number
                            cv2.rectangle(test_roi, (x, y), (x + w, y + h), (0, 255,0), 2)

                            # Sperate number and gibe prediction
                            curr_num = thre_mor[y:y+h,x:x+w]
                            curr_num = cv2.resize(curr_num, dsize=(digit_w, digit_h))
                            _, curr_num = cv2.threshold(curr_num, 220, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
                            crop_characters.append(curr_num)

                json_file = open('MobileNets_character_recognition.json', 'r')
                loaded_model_json = json_file.read()
                json_file.close()
                model = model_from_json(loaded_model_json)
                model.load_weights("License_character_recognition_weight.h5")
                print("[INFO] Model loaded successfully...")
                labels = LabelEncoder()
                labels.classes_ = np.load('license_character_classes.npy')
                print("[INFO] Labels loaded successfully...")
                final_string = ''
                for i,character in enumerate(crop_characters):
                    #fig.add_subplot(grid[i])
                    title = np.array2string(predict_from_model(character,model,labels))
                    # plt.title('{}'.format(title.strip("'[]"),fontsize=20))
                    final_string+=title.strip("'[]")
                    # plt.axis(False)
                    # plt.imshow(character,cmap='gray')

                

                end = timer()
                x = vehicle.shape[0]
                y = vehicle.shape[1]
                det_text="<br /><p style = 'color: blue;font-size:13px'> Image: <span>" + str(count)+"</span></p>"\
                "<p style = 'font-size:13px'>Inference time: <span>"+str(float("{:.2f}".format(end - start)))+"s</span></p>"\
                    "<p style = 'font-size:13px'>Raw Input Resolution: <span>"+str(y)+"X"+str(x)+"</span></p>"\
                        "<br />"

                det.markdown(det_text,unsafe_allow_html=True)
                flag = 1                
                if flag == 1:
                    box_img = draw_box(f, cor)
                    #print(box_img[0])
                    #print(vehicle[0])
                    st.image(box_img, use_column_width = True, clamp = True)
                    st.write('Predicted Plate Number: ',final_string)
            progress_text.text("Processed: "+str(count) + "/"+ str(len(files)) + " Images")
            progress_bar.progress(100)
            static_store.clear()
    else:
        static_store.clear()
 

main()
